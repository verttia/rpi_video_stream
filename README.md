# Minimal RTSP Server Launcher

## Dependencies

Install at least the following dependencies to build the program:

 - build-essential
 - cmake

 ###### Gstreamer must be built from source:

- install the following:

      sudo apt get python3-pip flex bison

- install build tools using pip:

      sudo pip3 install meson ninja

- clone gst-build repository and use branch 1.18:

      git clone -b https://gitlab.freedesktop.org/gstreamer/gst-build.git
      meson builddir

  If you only want to build necessary plugins, configure meson using:

      meson configure -Dgst-plugins-good:rpicamsrc=enabled -Dges=disabled -Dugly=disabled builddir

  Build the packages:

      ninja -C builddir

  Install:

      meson install -C builddir


## Enable Raspberry Pi Camera

Enable the camera by entering raspi-config:

    sudo raspi-config

Go to "3 Interface Options" and select "P1 Camera" > "Yes".

## Install 

    cmake .
    make
    make install

## Launch 

    ./start_rtsp_server.sh


## (Optional) Launch RTSP Server on Boot

Add new crontab:

    sudo crontab -e

Select editor and add the following at the bottom (replace the directory with this directory):

    @reboot sudo <this dir>/start_rtsp_server.sh

Save the file.
